const express = require("express");
const { createReservation } = require("../controllers/infos");
const router = express.Router();

router.route("/").post(createReservation);

module.exports = router;
