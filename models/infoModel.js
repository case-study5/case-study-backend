const mongoose = require("mongoose");

const infoModel = mongoose.Schema(
  {
    room: {
      type: String,
      required: true,
    },
    hotel: {
      type: Boolean,
      required: true,
    },
    sits: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

const Infos = mongoose.model("Infos", infoModel);

module.exports = Infos;
