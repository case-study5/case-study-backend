const express = require("express");
require("dotenv").config();
const cors = require("cors");
const connectDB = require("./config/db");

const infosRoutes = require("./routes/infosRoutes");

const app = express();
app.use(cors());
app.use(express.json());
connectDB();

app.use("/api", infosRoutes);

const PORT = process.env.PORT || 5000;
app.listen(PORT, console.log("Server on port 5000"));
