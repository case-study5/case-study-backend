const Infos = require("../models/infoModel");
const asyncHandler = require("express-async-handler");

const createReservation = asyncHandler(async (req, res) => {
  const { hotel, rooms, sits } = req.body;
  const infos = await Infos.create({
    room: rooms.label,
    hotel: hotel.label === "Oui" ? true : false,
    sits: sits.label,
  });
  if (infos) {
    res.status(200).json(infos);
  }
});

module.exports = { createReservation };
